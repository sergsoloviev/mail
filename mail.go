package mail

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"html/template"
	"log"
	"net/smtp"
	"strings"
)

type Email struct {
	User     string
	Password string
	Server   string
	Port     int
	To       []string
	Title    string
	Body     string
	Template string
	Tpl      *template.Template
	Buffer   *bytes.Buffer
}

func (o *Email) GoSend() {
	go o.Send()
}

func (o *Email) Send() {
	auth := smtp.PlainAuth("", o.User, o.Password, o.Server)
	addr := fmt.Sprintf("%s:%v", o.Server, o.Port)

	header := make(map[string]string)
	header["From"] = o.User
	header["To"] = strings.Join(o.To, ",")
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/html; charset=utf-8"
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += fmt.Sprintf(
		"%s: =?utf-8?B?%s?=\r\n",
		"Subject",
		base64.StdEncoding.EncodeToString([]byte(o.Title)),
	)
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(o.Body))

	err := smtp.SendMail(addr, auth, o.User, o.To, []byte(message))
	if err != nil {
		log.Println(err)
	}
}

func (o *Email) ParseTemplate(data interface{}) error {
	t, err := template.ParseFiles(o.Template)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return err
	}
	o.Body = buf.String()
	return nil
}
