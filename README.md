# package mail

```go

email := mail.Email{
    User:     "robot@myserver.com",
    Password: "robotsuperpassword",
    Server:   "smtp.server.com",
    Port:     587,
    To:       []string{"user@example.com"},
    Title:    "Test message",
    Template: "mail/mail.html",
}

data := map[string]string{"Name": "test1", "URL": "http://example.com"}
email.ParseTemplate(data)
email.GoSend()

```
